<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
    <link rel="stylesheet" href="../css/login.css">
    <title>Login</title>
</head>
<body>
    <main>
        <h1>Login</h1>
        <form action="../controllers/verifica_login.php" method="post">
            <label for="usuario">usuario</label>
            <input type="text" id="usuario" name="usuario">
            <label for="senha">Senha</label>
            <input type="password" id="senha" name="senha">
            <input type="submit" class="btn" value="Fazer login">
        </form>
    </main>
</body>
</html>