<?php

require_once '../models/Usuario.php';

$usuario = new Usuario;

$lista = $usuario->listar();

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
    <title>Lista de Usuarios</title>
</head>
<body>
    <main>
        <h1 style="text-align: center; margin-bottom:1em;">Lista de Usuarios cadastrados</h1>
        <table>
            <tr>
                <th>id</th>
                <th>nome</th>
                <th>senha</th>
            </tr>
            <tr>
            <?php foreach($lista as $linha): ?>
            
            <tr  class="border border-dark">
                <td><?php echo $linha['id']?></td>
                <td><?php echo $linha['nome']?></td>
                <td><?php echo $linha['senha']?></td>
            </tr>
            <?php endforeach ?>
            </tr>
        </table>
    </main>
</body>
</html>